import numpy     as np
import pandas    as pd
import folium
import streamlit as st
from streamlit_folium import folium_static
from folium.plugins import MarkerCluster
import geopandas
import plotly.express as px
from datetime import datetime

st.set_page_config( layout='wide' )

@st.cache( allow_output_mutation=True )
def get_data( path ):
    data = pd.read_csv( 'datasets/kc_house_data.csv')
    return data

@st.cache( allow_output_mutation=True )
def get_geofile( url ):
    geofile = geopandas.read_file( url )

    return geofile

#get data
path = 'datasets/kc_house_data.csv'
data = get_data( path )

#get geofile
url = 'https://data-seattlecitygis.opendata.arcgis.com/datasets/83fc2e72903343aabff6de8cb445b81c_2.geojson'
geofile = get_geofile( url )


#add new feature
data['price_m2'] = data['price'] / data['sqft_lot']

# ==================
#   Data Overview
# ==================

#criando filtros
f_attributes = st.sidebar.multiselect( 'Enter columns', data.columns )
f_zipcode = st.sidebar.multiselect( 'Enter zipcode', data['zipcode'].unique() )



if ( f_zipcode != [] ) & (f_attributes != []):
    data = data.loc[data['zipcode'].isin( f_zipcode ), f_attributes]

elif ( f_zipcode != [] ) & (f_attributes == []):
    data = data.loc[data['zipcode'].isin( f_zipcode ), :]

elif ( f_zipcode == [] ) & (f_attributes != []):
    data = data.loc[:, f_attributes]

else:
    data = data.copy()

#titulo no streamlit
st.title( 'Data Overview')
st.dataframe( data )

#Average metrics
df1 = data[['id', 'zipcode']].groupby( 'zipcode' ).count().reset_index()
df2 = data[['price', 'zipcode']].groupby( 'zipcode' ).mean().reset_index()
df3 = data[['sqft_living', 'zipcode']].groupby( 'zipcode' ).mean().reset_index()
df4 = data[['price_m2', 'zipcode']].groupby( 'zipcode' ).mean().reset_index()
m1 = pd.merge( df1, df2, on='zipcode', how='inner')
m2 = pd.merge( m1, df3, on='zipcode', how='inner')
df = pd.merge( m2, df4, on='zipcode', how='inner')

#renomeando colunas
df.columns= ['ZIPCODE', 'TOTAL_HOUSES', 'PRICE', 'sqrt SQRT LIVING', 'PRICE/M2']

#Estatistica Descritiva

# descriptive statistics
num_attributes = data.select_dtypes( include=['int64', 'float64'] )

# central tendency
media = pd.DataFrame( num_attributes.apply( np.mean ) )
mediana = pd.DataFrame( num_attributes.apply( np.median ) )
std = pd.DataFrame( num_attributes.apply( np.std ) )

# dispersion
std = pd.DataFrame( num_attributes.apply( np.std ) )
max_ = pd.DataFrame( num_attributes.apply( np.max ) )
min_ = pd.DataFrame( num_attributes.apply( np.min ) )

df1 = pd.concat( [max_, min_, media, mediana, std ], axis=1 ).reset_index()

pd.set_option('display.float_format', lambda x: '%.2f' % x)
df1.columns = ['attributes', 'max', 'min', 'mean', 'median', 'std']

c1, c2 = st.beta_columns(( 1, 1 ))
c1.header( 'Average Values' )
c1.dataframe( df, height=600 )
c2.header( 'Estatística Descritiva' )
c2.dataframe( df1, height=600 )


#========================
#Densidade de Portifólio
#========================

st.title( 'Region Overview' )

c1, c2 = st.beta_columns( (1, 1) )
c1.header( 'Portifolio Density' )

df = data.sample( 10 )

#Construção do mapa
density_map = folium.Map( location=[data['lat'].mean(), data['long'].mean()],
                          default_zoom_start=15 )
marker_cluster = MarkerCluster().add_to( density_map )
for name, row in df.iterrows():
    folium.Marker( [ row['lat'], row['long'] ], popup='Sold R${0} on: {1} Features: {2} Sqft {3} Bedrooms {4} Bathrooms Year Built {5}'.format( row['price'], row['date'], row['sqft_living'], row['bedrooms'], row['bathrooms'], row['yr_built']) ).add_to( marker_cluster)

with c1:
    folium_static( density_map )


# Region price map

c2.header( 'Price Density' )

df = data[['price', 'zipcode']].groupby('zipcode').mean().reset_index()
df.columns = ['ZIP', 'PRICE']

#df = df.sample( 10 )

geofile = geofile[geofile['ZIP'].isin( df['ZIP'].tolist())]

region_price_map = folium.Map( location=[data['lat'].mean(), data['long'].mean()], default_zoom_start=15 )

region_price_map.choropleth( data = df, geo_data = geofile, columns=['ZIP', 'PRICE'], key_on='feature.properties.ZIP', fill_color='YlOrRd', fill_opacity = 0.7, line_opacity = 0.2, legend_name='AVG PRICE' )

with c2:
    folium_static( region_price_map)


#===================================================
#Distribuição dos imóveis por categorias comerciais
#===================================================

st.sidebar.title('Comercial Options')
st.title('Comercial Attributes')

#Average Price Year
data['date'] = pd.to_datetime(data['date']).dt.strftime('%Y-%m-%d')

#filters
min_year_built = int( data['yr_built'].min() )
max_year_built = int( data['yr_built'].max() )
st.sidebar.subheader( 'Select Max Year Built' )
f_year_built = st.sidebar.slider( 'Year Built', min_year_built, max_year_built, min_year_built )

st.header( 'Average Price per Year Built' )

df = data.loc[data['yr_built'] < f_year_built]
df = df[['yr_built', 'price']].groupby('yr_built').mean().reset_index()
fig = px.line(df, x='yr_built', y='price')
st.plotly_chart( fig, use_container_width=True )

#Average Price Day
st.header('Average Price per Day')
st.sidebar.subheader( 'Select Max Date' )

#filters
min_date = datetime.strptime( data['date'].min(),'%Y-%m-%d' )
max_date = datetime.strptime( data['date'].max(),'%Y-%m-%d')

f_date = st.sidebar.slider( 'Date', min_date, max_date, min_date )

data['date'] = pd.to_datetime( data['date'] )
df = data.loc[data['date'] < f_date]
df = df[['date', 'price']].groupby('date').mean().reset_index()
fig = px.line(df, x='date', y='price')
st.plotly_chart( fig, use_container_width=True )


#===========
#Histograma
#===========
st.header( 'Price Distribution')
st.sidebar.subheader('Select Max Price')

min_price = int( data['price'].min())
max_price = int( data['price'].max())
avg_price = int( data['price'].mean())

f_price = st.sidebar.slider( 'Price', min_price, max_price, avg_price)
df = data.loc[data['price'] < f_price]

fig = px.histogram(df, x='price', nbins=50)
st.plotly_chart( fig, use_container_width=True )


#===============================================
#Distribuição dos imóveis por categorias físicas
#===============================================

st.sidebar.title( 'Attributes Options')
st.title( 'House Attributes' )

#filters
f_bedrooms = st.sidebar.selectbox( 'Max Number of Bedrooms', sorted( set( data['bedrooms'].unique() ) ) )
f_bathrooms = st.sidebar.selectbox( 'Max Number of Bathrooms', sorted( set( data['bathrooms'].unique() ) ) )

c1, c2 = st.beta_columns(2)

#House per bedrooms
c1.header('Houses per bedrooms')
df = data[data['bedrooms'] < f_bedrooms]
fig = px.histogram( df, x='bedrooms', nbins=19 )
c1.plotly_chart( fig, use_container_width=True)

#House per bathrooms
c2.header('Houses per Bathrooms')
df = data[data['bathrooms'] < f_bathrooms]
fig = px.histogram( df, x='bathrooms', nbins=19 )
c2.plotly_chart( fig, use_container_width=True)

# filters
f_floors = st.sidebar.selectbox('Max number of floors', sorted( set( data['floors'].unique() ) ) )
f_waterview = st.sidebar.checkbox('Only House with Water View' )

c1, c2 = st.beta_columns( 2 )

# Houses per floors
c1.header( 'Houses per floors' )
df = data[data['floors'] < f_floors]
fig = px.histogram( df, x='floors', nbins=19 )
c1.plotly_chart( fig, use_containder_width=True )

# Houses per water view
if f_waterview:
    df = data[data['waterfront'] == 1]
else:
    df = data.copy()
    
fig = px.histogram( df, x='waterfront', nbins=10 )
c2.header( 'Houses per water view' )
c2.plotly_chart( fig, use_containder_width=True )








































